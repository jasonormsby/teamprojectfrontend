import React, { useState,useEffect,useContext  } from "react";
import ReactDOM from 'react-dom';

import MyComponent from './MyComponent'
import {increment,decrement, getState,showStateClaims,showSubClaim} from './MySlice';
import { useSelector, useDispatch } from 'react-redux';
import ClaimsUpdateForm from './ClaimsUpdateForm'


function ClaimsList(props){

    const[claims,setClaims] = useState([]);
    const[isLoaded,setLoaded] = useState(false);
    const[addClaims, setAddClaims] = useState(false);

    useEffect(() =>{
        fetch(`http://localhost:8080/claims/get/${props.id}`)
            .then(res => res.json())
            .then( (result) => {
                    setClaims(result);
                    setLoaded(true);
                    console.log(result)
                },
                (error) => {
                    console.log(error)
                    setLoaded(true);
                }
            )
    }, [])

  //bind initial state
  const count = useSelector(getState);
  
  //for bind setter
  const dispatch = useDispatch();

if(count ==3 || count==5){
    return(
        <div>



            <ul class="list-group">
                {claims.map(
                    (arrayItem) => (
                        <li class ='list-group-item' key={arrayItem.id}>

                            <br/>
                            <span> Claim id: </span>


                            {arrayItem.id}



 


                            <br/>
                            <span>Claimtype: </span>
                            {arrayItem.claimType}

                            <br/>
                            <span>ClaimNo: </span>
                            {arrayItem.claimNumber}

                            <br/>
                            <span>ClaimDate: </span>
                            {arrayItem.claimDate}

                          <br></br>
                            <span>Claim Amount: </span>
                            {arrayItem.claimAmount}

                            <br/>
                            <span>ClaimStatus: </span>
                            {arrayItem.claimStatus}


                            <ClaimsUpdateForm id={arrayItem.id}/> 

                            <li class ='list-group-item'>

                            <button class="bi bi-search" onClick={ ()=>{ dispatch(showSubClaim())
 
}}> Update Claim   </button>

                            


                            </li>
                        </li>)
                )
                }
            </ul>




        </div>
    )}
    else{
return ""

    }

}

export default ClaimsList