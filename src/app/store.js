import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../MySlice';

export const store = configureStore({
  reducer: {
    counter: counterReducer,
  },
});
