import './App.css';
//react and javascript hook modules
import React, { useState,useEffect,useContext  } from "react";
import ReactDOM from 'react-dom';
import {increment, getState} from './MySlice';
import { useSelector, useDispatch } from 'react-redux';

function ClaimsForm(props){


    //hooks so user can alter our states/variables
    const [id, setFirstName] = useState("");
    const [amount, setLastName] = useState("");  
    const [number, setCity] = useState("");  
    const [status, setZip] = useState("");  
    const [type, setType] = useState("");  

    //POST on submission
    function formHandler(form){
        


        let targetUri = 
`http://localhost:8080/claims/create?id=${id}&amount=${amount}&number=${number}&status=${status}&type=${type}`
        let options = { method: 'POST'}
        //headers: { 'Content-Type': 'application/json' }

        fetch(targetUri,options).then(res => res.json()).then( (result) => { console.log("customer added") })
        console.log("got it")
    }


 //bind initial state
 const count = useSelector(getState);

if(count == 2){
        return(
        <div class="w-25 p-3" >
            <form class="form-group" onSubmit={formHandler}>
                <label>Customer Id </label>
                <input class="form-control"  value={id} onChange={ (e)=>{setFirstName(e.target.value)} }></input>
                <label>Claim Amount ($)</label>
                <input class="form-control"  value={amount} onChange={ (e)=>{setLastName(e.target.value)} }></input>
                <label>Number of Claim</label>
                <input class="form-control"  value={number} onChange={ (e)=>{setCity(e.target.value)} }></input>
                <label>Current Status Of Claim</label>
                <input class="form-control"  value={status} onChange={ (e)=>{setZip(e.target.value)} }></input>
                <label>Type of Claim</label>
                <input class="form-control"  value={type} onChange={ (e)=>{setType(e.target.value)} }></input>
                <button class="btn btn-secondary"  type="submit">Create Claim</button>
            </form>
        </div>
        )
    }
    else{return <div></div>}
}



export default ClaimsForm;