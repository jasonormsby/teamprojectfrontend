import './App.css';
//react and javascript hook modules
import React, { useState,useEffect,useContext  } from "react";
import ReactDOM from 'react-dom';
import {increment, getState} from './MySlice';
import { useSelector, useDispatch } from 'react-redux';

function ClaimsUpdateForm(props){


    //hooks so user can alter our states/variables
    const [claimType, setClaimType] = useState("");
    const [claimNumber, setClaimNumber] = useState("");
    const [claimDate, setClaimDate] = useState("");
    const [claimAmount, setClaimAmount] = useState("");
    const [claimStatus, setClaimStatus] = useState("");

    //POST on submission
    function formHandler(form){

        let targetUri =
            `http://localhost:8080/claims/update?cType=${claimType}&CNumber=${claimNumber}&Date=${claimDate}&Amount=${claimAmount}&Status=${claimStatus}`
        let options = { method: 'POST'}
        //headers: { 'Content-Type': 'application/json' }

        fetch(targetUri,options).then(res => res.json()).then( (result) => { console.log("customer added") })
        console.log("got it")
    }


    //bind initial state
    const count = useSelector(getState);

    if(count ==5){
        return(
            <div class="w-25 p-3" >
                <form class="form-group" onSubmit={formHandler}>
                    <label>Claim Type</label>
                    <input  class="form-control"  value={claimType} onChange={ (e)=>{setClaimType(e.target.value)} }></input>
                    <label>Claim Number</label>
                    <input  class="form-control"  value={claimNumber} onChange={ (e)=>{setClaimNumber(e.target.value)} }></input>
                    <label>Claim Date</label>
                    <input  class="form-control"  value={claimDate} onChange={ (e)=>{setClaimDate(e.target.value)} }></input>
                    <label>Claim Amount</label>
                    <input  class="form-control"  value={claimAmount} onChange={ (e)=>{setClaimAmount(e.target.value)} }></input>
                    <label>Claim Status</label>
                    <input  class="form-control"  value={claimStatus} onChange={ (e)=>{setClaimStatus(e.target.value)} }></input>
                    <button type="submit">Submit Claim Update</button>
                </form>
            </div>
        )
    }
    else{return <div></div>}
}



export default ClaimsUpdateForm;