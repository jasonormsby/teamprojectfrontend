//react and javascript hook modules
import React, { useState,useEffect,useContext  } from "react";
import ReactDOM from 'react-dom';
import {increment,decrement, getState,showStateClaims,showCustomerUpdate} from './MySlice';
import { useSelector, useDispatch } from 'react-redux';
import MyComponent from './MyComponent'
import ClaimsList from './ClaimsList'
import CustomerUpdateForm from './CustomerUpdateForm'


function CustomerList(){

  
  const [claims, setClaims] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [customerId, setCustomerId] = useState("");
  const [isLoaded, setLoaded] = useState(false);
  const [addingCustomer, setAddingCustomer] = useState(false);

  //on load of component
  useEffect(() => {
  fetch("http://localhost:8080/customer/all/").then(res => res.json())
  .then( (result) => {
    setCustomers(result);
    //setLoaded(true);
    console.log(result)
  },
  (error) => {
    console.log(error)
    setLoaded(true);
    //this.setState({ error    })
  })
  
  }, [])


  function myButtonHandler(e) {
    //this is very important
    e.preventDefault();
    console.log('You clicked submit.');

  }
  
  function deleteHandler(e) {
    //e.preventDefault();
    //e.preventDefault();
    console.log(e)
    let targetUri = 
            `http://localhost:8080/customer/delete/${e}`
    let options = { method: 'DELETE'}
    //headers: { 'Content-Type': 'application/json' }

    fetch(targetUri,options).then(res => res.json()).then( (result) => { console.log("customer DELETED") })
    console.log("got it")

  }


  //bind initial state
  const count = useSelector(getState);
  
  //for bind setter
  const dispatch = useDispatch();
  


  if(true){
  
 


    return(
      <div>



        <ul class="list-group">
        {customers.map( 
          (arrayItem) => (
            <li class ='list-group-item' key={arrayItem.id}>
            
            <br/>
            <span> Customer id: </span>
            
            
            {arrayItem.id} 

           
            
   

  

            
            <br/>
            <span>Name: </span>
            {arrayItem.firstName}
            <span> </span>
            {arrayItem.lastName} 
            
            <br/>
            <span>City: </span>
            {arrayItem.city} 
            
            <br/>
            <span>Zip: </span>
            {arrayItem.zip}
            
            <li class ='list-group-item'>
        
           
            <button class="bi bi-search" onClick={ ()=>{ dispatch(showStateClaims())
 
                }}> View customer claims </button>

<button class="bi bi-search" onClick={ ()=>{ dispatch(showCustomerUpdate())
 
}}> Update customer   </button>


            <button class="bi bi-trash" onClick={ ()=>{deleteHandler(arrayItem.id)
             window.location.reload();
                }}> Delete customer </button>
         
  

      <CustomerUpdateForm id={arrayItem.id}/>
       <ClaimsList id={arrayItem.id}/> 
        
                </li>
          </li>)
          ) 
        }
        </ul>


     

      </div>
    )
  }
}

export default CustomerList

