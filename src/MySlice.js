import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';



//initial context
const initialState = {
    value: 0,
    status: 'idle',
  };

//A function that accepts an initial state, an object full of reducer functions
export const counterSlice = createSlice({
  
  //slice name
  name: 'counter',
  
  //init fields
  initialState,

  // reducer functions modify state
  reducers: {
    increment: (state) => {
      state.value = 1;
      console.log(state.value)
    },
    decrement: (state) => {
      state.value = 2;
      console.log(state.value)
    },
    showStateClaims: (state) => {
      state.value = 3;
      console.log(state.value)
    },
    showCustomerUpdate: (state) => {
      state.value = 4;
      console.log(state.value)
    },
    showSubClaim: (state) => {
      state.value = 5;
      console.log(state.value)
    },
  }

});


//allow for retrival of state
export const getState = (state) => state.counter.value;

//setter for state
export const { increment,decrement,showStateClaims,showCustomerUpdate,showSubClaim  } = counterSlice.actions;

//reducer
export default counterSlice.reducer;
