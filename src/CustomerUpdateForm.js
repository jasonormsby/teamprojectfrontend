import './App.css';
//react and javascript hook modules
import React, { useState,useEffect,useContext  } from "react";
import ReactDOM from 'react-dom';
import {increment, getState} from './MySlice';
import { useSelector, useDispatch } from 'react-redux';


function CustomerUpdateForm(props){

    //Gets the id from the props
    let id = props.id

    //hooks so the user can alter the state/vars
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [city, setCity] = useState("");
    const [zip, setZip] = useState("");

    //on load of component gets the id of the customer
    useEffect(() => {
        fetch(`http://localhost:8080/customer/get/${id}`).then(res => res.json())
            .then( (result) => {
                    setFirstName(result.firstName)
                    setLastName(result.lastName)
                    setCity(result.city)
                    setZip(result.zip)
                    console.log(result)
                },
                (error) => {
                    console.log(error)
                    //this.setState({ error    })
                })

    }, [])

    //PUT on submission
    function formHandler(form){

        console.log(id)
//configured to get the id of the clicked customer
let targetUri =
`http://localhost:8080/customer/update?id=${id}&fName=${firstName}&lName=${lastName}&city=${city}&zip=${zip}`

let options = {method: 'PUT'}

fetch(targetUri,options).then(res => res.json()).then( (result) => { console.log("customer updated") })
console.log("customer updated")





    }

    const count = useSelector(getState)
    if(count == 4){
        return(
            <div class="w-25 p-3" >
                <form class="form-group" onSubmit={formHandler}>
                    <label>First Name</label>
                    <input class="form-control" value={firstName} onChange={ (e)=>{setFirstName(e.target.value)} }></input>
                    <label>Last Name</label>
                    <input class="form-control" value={lastName} onChange={ (e)=>{setLastName(e.target.value)} }></input>
                    <label>City</label>
                    <input class="form-control" value={city} onChange={ (e)=>{setCity(e.target.value)} }></input>
                    <label>Zip</label>
                    <input class="form-control" value={zip} onChange={ (e)=>{setZip(e.target.value)} }></input>
                    <button class="btn btn-secondary" type="submit">Update Customer</button>
                </form>
            </div>
        )
    }
    else{return <div></div>}
}
export default CustomerUpdateForm;