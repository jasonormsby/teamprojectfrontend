import './App.css';
//react and javascript hook modules
import React, { useState,useEffect,useContext  } from "react";
import ReactDOM from 'react-dom';
import {increment, getState} from './MySlice';
import { useSelector, useDispatch } from 'react-redux';

function CustomerForm(props){


    //hooks so user can alter our states/variables
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");  
    const [city, setCity] = useState("");  
    const [zip, setZip] = useState("");  

    //POST on submission
    function formHandler(form){
        
        let targetUri = 
                `http://localhost:8080/customer/add?fName=${firstName}&lName=${lastName}&city=${city}&zip=${zip}`
        let options = { method: 'POST'}
        //headers: { 'Content-Type': 'application/json' }

        fetch(targetUri,options).then(res => res.json()).then( (result) => { console.log("customer added") })
        console.log("got it")
    }


 //bind initial state
 const count = useSelector(getState);

if(count ==1){
        return(
        <div class="w-25 p-3" >
            <form class="form-group" onSubmit={formHandler}>
                <label>First Name</label>
                <input class="form-control" value={firstName} onChange={ (e)=>{setFirstName(e.target.value)} }></input>
                <label>Last Name</label>
                <input class="form-control" value={lastName} onChange={ (e)=>{setLastName(e.target.value)} }></input>
                <label>City</label>
                <input class="form-control" value={city} onChange={ (e)=>{setCity(e.target.value)} }></input>
                <label>Zip</label>
                <input class="form-control" value={zip} onChange={ (e)=>{setZip(e.target.value)} }></input>
                <button class="btn btn-secondary" type="submit">Create Customer</button>
            </form>
        </div>
        )
    }
    else{return <div></div>}
}



export default CustomerForm;