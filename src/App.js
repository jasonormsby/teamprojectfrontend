
import './App.css';
//react and javascript hook modules
import React, { useState,useEffect } from "react";
import ReactDOM from 'react-dom';

//import MyComponent from './MyComponent'

import { MyComponent } from './MyComponent';
import CustomerForm from './CustomerForm'
import CustomerList from './CustomerList'
import ClaimsForm from './ClaimsForm';

function App() {

  const [error, setError] = useState("");
 
 
  const [customers, setCustomers] = useState([]);
  const [isLoaded, setLoaded] = useState(false);




  //on load of component
  useEffect(() => {
  fetch("http://localhost:8080/customer/all/").then(res => res.json())
  .then( (result) => {
    setCustomers(result);
    setLoaded(true);
    console.log(result)
  },
  (error) => {
    console.log(error)
    setLoaded(true);
    setError(error)
  })
  
  }, [])


  function myButtonHandler(e) {
    //this is very important
    e.preventDefault();
    console.log('You clicked submit.');
    setAddingCustomer(true)

  }
  

  let userAction = {
    addingCustomer:false
  }

  const [addingCustomer, setAddingCustomer] = useState(false);

  function myButtonHandler(e) {
    //this is very important
    e.preventDefault();
    console.log('You clicked submit.');
  }

    return (
    <div name="container" class="container-sm">
      <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
        crossorigin="anonymous"
      />
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css"></link>
     

      <MyComponent/>
     
      <CustomerForm/>
 
      <ClaimsForm/>
     
      <CustomerList />
    


    </div>


    )





}




export default App;
